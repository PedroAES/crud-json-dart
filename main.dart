import 'dart:convert';

import 'package:untitled1/untitled1.dart' as untitled1;
import 'dart:io';

main (List<String> arguments) async {
  var dado;
  do {
    print('Olá. Escolha uma opção');
    print('1 - Inserir Contato');
    print('2 - Editar Contato');
    print('3 - Excluir Contato');
    print('4 - Listar Contatos');
    print('0 - Sair');
    dado = stdin.readLineSync();

    switch(dado){
      case '1':
        await ContatosController.addContatoToJson();
        break;
      case '2':
        break;
      case '3':
        await ContatosController.removeContatoFromJson();
        break;
      case '4':
        print('Listando contatos...');
        await ContatosController.showAllContatos();
        break;
      case '0':
        print('Saindo...');
        break;
      default:
        print('Escolha uma opção válida');
        break;
    }
  } while (dado!='0');
}

class Contato {
  String _nome;
  String _apartamento;

  Contato(this._nome, this._apartamento);

  String get nome => _nome;

  String get apartamento => _apartamento;

  Contato.fromJson(Map<String, dynamic> json)
      :
        _nome = json['nome'],
        _apartamento = json['apartamento'];

  Map<String, dynamic> toJson() =>
      {
        'nome': _nome,
        'apartamento': _apartamento,
      };
}

class ListContatos{
  List<Contato> _contatos = List();

  ListContatos(this._contatos);

  List<Contato> get contatos => _contatos;

  void addContato(Contato contato){
    _contatos.add(contato);
  }

  void removeContato(String apt){
    this._contatos.removeWhere((contatos) => contatos.apartamento == apt);
  }

  void listarContatos(){
    for(int i=0; i<contatos.length; i++){
      print('\ncontato $i');
      print(contatos[i].nome);
      print(contatos[i].apartamento);
    }
  }

  factory ListContatos.fromJson(List<dynamic> parsedJson) {
    List<Contato> contatos = List<Contato>();
    contatos = parsedJson.map((i) => Contato.fromJson(i)).toList();
    return ListContatos(contatos);
  }

  Set<List<Contato>> toJson() => {_contatos};
}


class ContatosController {
  static void showAllContatos() async {
    ListContatos cont = await getListContatos();
      cont.listarContatos();
  }

  static void addContatoToJson() async {
    ListContatos contatos = await getListContatos();
    Contato contato = newContato();
    contatos.addContato(contato);
    writeOnJson(contatos);
  }

  static void removeContatoFromJson() async {
    ListContatos contatos = await getListContatos();
    stdout.write("\n\nDigite o apartamento que deseja deletar:\n");
    contatos.removeContato(stdin.readLineSync());
    writeOnJson(contatos);
  }

  static Contato newContato() {
    print('Digite o nome do contato');
    String nome = stdin.readLineSync();
    print('Digite o apartamento de $nome');
    String apt = stdin.readLineSync();
    print('Inserido com sucesso');

    return Contato(nome, apt);
  }

  static Future<ListContatos> getListContatos() async {
    Future<ListContatos> contatos =readContatosFromJson();
    ListContatos conts;
    await contatos
        .then((result) => conts = ListContatos(result.contatos));
    return conts;
  }

  static void writeOnJson(ListContatos conts) {
    var str = json.encode(conts.contatos);
    File file = File("./assets/contatos_list.json");
    file.writeAsStringSync(str);
  }

  static Future<ListContatos> readContatosFromJson() async {
    String jsonString = await loadJsonAsset();
    final jsonResponse = json.decode(jsonString);
    ListContatos contatos = ListContatos.fromJson(jsonResponse);
    return contatos;
  }

  static Future<String> loadJsonAsset() async {
    return await File('./assets/contatos_list.json').readAsString();
  }

}


